# The Box Model

The most fundamental thing in CSS is the Box Model, because all HTML elements can be considered as boxes.

The CSS box model is essentially a box that wraps around every HTML element. The CSS box model consists of:

- margins
- borders
- padding and
- actual content.

The box starts with the content that lives in the box.

Intrinsic and extrinsic is core to how you think about your layouts.

## Content Box

### Intrinsic Sizing and Layout

Shrink-wrapped, the content affects the content size.
Ask the content how much space it needs.
The text can change and the box is dynamic and can change, as it looks in itself to change.

### Extrinsic Sizing and Layout

We have a photo of a flower, in 4:3 landscape ratio layout. We have a box, with dimensions of 200 by 200 pixels. While the box was predefined, the content now needs to fit into the box and is told how to fit in the box.

Content is told how to fit. We explicitly set the size and the content must fit in my zone.

## Padding

Padding lives between content and the Border Box.

## Border Box

Border Box lives between Padding and the Outline and Box Shadow.

## Outline and Box Shadow

## Margin Box

Notice in both examples the margin is in the white. Margin is unique in that it doesn’t affect the size of the box itself per se, but it affects other content interacting with the box, and thus an important part of the CSS box model.

## Links

1. [The CSS Podcast: The Box Model](https://podcasts.google.com/?feed=aHR0cHM6Ly90aGVjc3Nwb2RjYXN0LmxpYnN5bi5jb20vcnNz&episode=M2RhNDg5Y2QtZGVkZS00ZDIyLTk2ZTYtYmU1ZmZkNDhmY2I4&ved=0CBMQzsICahcKEwiwq6jbt__oAhUAAAAAHQAAAAAQBw)

2. [Adam Argyle's Box Model on CodePen](https://codepen.io/argyleink/pen/bGNmgGW)

3. [CSS Box Model Module Level 3](https://www.w3.org/TR/css-box-3/)

4. [CSS Tricks: The Box Model](https://css-tricks.com/the-css-box-model/)
