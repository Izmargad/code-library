
# Simple steps to run and compile SCSS to CSS

 Note: Make sure you have installed node in your system. If you want to help to install node js based on your system then check our other tutorial or check node js official website.

## Step 1

Now create a blank folder and open  terminal(linux) or cmd(windows) and navigate to your current project folder by using cd command

## Step 2

Now run below command

npm init

### Step 2a Set your Project up as per normal

after enter it will ask you some package info that you can fill according to you or just keep enter until it finished.
The above command will generate package.json file

## Step 3

Now  we will install npm module that will convert our scss to css

Run below command:

npm install node-sass

So we have installed node-sass package .

Now open package.json file in your editor and add below code into it into script object

```javascript
"scss": "node-sass --watch scss -o css"
````

After adding above line your code will be look like this

```javascript
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1",
  "scss": "node-sass --watch scss -o css"
}
```

and your package.json wil be look like this

```javascript
{
  "name": "scss",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "scss": "node-sass --watch scss -o css"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "node-sass": "^4.13.0"
  }

}
```

Now create two folders scss and css and create a file styles.scss into scss folder and styles.css

After that your directory will be look like this
css

- styles.css
node_modules
scss
- styles.scss
package.json

## Step 4

Now run below command in your root directory

npm run scss

This command will watch our scss files changes and make generate css for it into styles.css file

## Step 5

Now open styles.scss file and styles.css file

Now add some scss code into styles.scss like below

```css
$colortype : red;

.clr {

color: $colortype

}
```

and after saving this file you can see your css file have css code for this scss dynamically so you dont need to run command again and again , now you can write your scss code and node will generate css code automatically.
