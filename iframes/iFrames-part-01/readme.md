# Post Data to an IFrame

Doesn't take any JavaScript or anything. You just have the form's target attribute match the iframe's name attribute.

```html
<form action="iframe.php" target="my-iframe" method="post">
   
  <label for="text">Some text:</label>
  <input type="text" name="text" id="text">
   
  <input type="submit" value="post">
   
</form>
  
<iframe name="my-iframe" src="iframe.php"></iframe>
```

The outer page doesn't even reload. But it might appear to at first glance since many browsers run the page-loading spinner in the tab when an iframe reloads.

*Very useful for asynchronous posting files.*
