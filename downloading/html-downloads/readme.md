# Automatically assign a filename when using the HTML download attribute to download a file

Last month, we looked at how to force a file to download instead of open in the browser using only HTML

It’s as simple as adding the download attribute to a link that points to the file.

```html
<a href="/path/to/your/receipt.pdf" download>Download Receipt</a>
```

Reader Belkin Fahri reminded me of something I neglected to mention: if you assign a value to the download attribute, that value becomes the downloaded file’s name.

In our example above, instead of receipt.pdf, let’s say we wanted the PDF to download as magic-wand-purchase.pdf. We would use magic-wand-purchase.pdf as the value for the download attribute.

```html
<a href="/path/to/your/receipt.pdf" download="magic-wand-purchase.pdf">Download Receipt</a>
```

[Go Make Things: 18 March 2020](https://gomakethings.com/how-to-automatically-assign-a-filename-when-using-the-html-download-attribute-to-download-a-file/)
