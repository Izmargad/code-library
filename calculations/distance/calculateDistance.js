var
  svg = document.getElementById('mysvg'),
  NS = svg.getAttribute('xmlns'),
  local = svg.getElementById('local'),
  coords = document.getElementById('coords');

// update co-ordinate output
svg.addEventListener('mousemove', function(e) {

  var
    x = e.clientX,
    y = e.clientY,
    svgP = svgPoint(svg, x, y),
    svgL = svgPoint(local, x, y);

  // output co-ordinates
  coords.textContent =
    '[page: ' + x + ',' + y +
    '] => [svg space: ' + Math.round(svgP.x) + ',' + Math.round(svgP.y) +
    '] [local transformed space: ' + Math.round(svgL.x) + ',' + Math.round(svgL.y) + ']'
    ;

}, false);

// add a circle to the SVG
svg.addEventListener('click', function(e) {
  
  var
    t = e.target,
    x = e.clientX,
    y = e.clientY,
    target = (t == svg ? svg : t.parentNode),
    svgP = svgPoint(target, x, y),
    circle = document.createElementNS(NS, 'circle');
  
  circle.setAttributeNS(null, 'cx', Math.round(svgP.x));
  circle.setAttributeNS(null, 'cy', Math.round(svgP.y));
  circle.setAttributeNS(null, 'r', 10);
  target.appendChild(circle);

  console.log(coords);
  
}, false);

// translate page to SVG co-ordinate
function svgPoint(element, x, y) {
  
  var pt = svg.createSVGPoint();
  pt.x = x;
  pt.y = y;

  
  //console.log(y);

  return pt.matrixTransform(element.getScreenCTM().inverse());
  
  
}