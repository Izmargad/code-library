# CSS Selectors

CSS uses Selectors for binding style properties to elements in the document.

Selectors are patterns that match against elements in a tree, and as such form one of several technologies that can be used to select nodes in an XML document. Selectors have been optimized for use with HTML and XML, and are designed to be usable in performance-critical code.

## ID Selectors

## Class Selectors

## Pseudo Classes

## Tag Selector

## Attribute Selector

## Positional Selectors

## Other Pseudo Selectors
