const URL = `https://random-acts-of-friendliness.netlify.com/.netlify/functions/random`;

const textarea = document.querySelector("#words");

function getWords() {
  fetch(URL)
    .then(response => response.text())
    .then(data => {
      textarea.value = data;
    });
}

document.getElementById("get-button").addEventListener("click", getWords);

getWords();
