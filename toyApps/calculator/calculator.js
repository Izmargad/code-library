let runningTotal = 0;
let buffer = "0";
let previousOperator;
const screen = document.querySelector(".screen");

/*
var length = 20;

var text = document.getElementById('text')
var string = text.innerHTML
var trimmedString = string.length > length ?
  string.substring(0, length - 3) + "..." :
  string

text.innerHTML = trimmedString
*/

function buttonClick(value) {
  if (isNaN(parseInt(value))) {
    handleSymbol(value);
  } else {
    handleNumber(value);
  }
  rerender();
}

function handleNumber(value) {
  if (buffer === "0") {
    buffer = value;
  } else {
    buffer += value;
  }
}

function handleMath(value) {
  if (buffer === "0") {
    // do nothing
    return;
  }

  const intBuffer = parseInt(buffer);
  if (runningTotal === 0) {
    runningTotal = intBuffer;
  } else {
    flushOperation(intBuffer);
  }

  previousOperator = value;

  buffer = "0";
}

function flushOperation(intBuffer) {
  if (previousOperator === "+") {
    runningTotal += intBuffer;
  } else if (previousOperator === "-") {
    runningTotal -= intBuffer;
  } else if (previousOperator === "×") {
    runningTotal *= intBuffer;
  } else {
    runningTotal /= intBuffer;
  }
}

function handleSymbol(value) {
  switch (value) {
    case "C":
      buffer = "0";
      runningTotal = 0;
      break;
    case "=":
      if (previousOperator === null) {
        // need two numbers to do math
        return;
      }
      flushOperation(parseInt(buffer));
      previousOperator = null;
      buffer = +runningTotal;
      runningTotal = 0;
      break;
    case "←":
      if (buffer.length === 1) {
        buffer = "0";
      } else {
        buffer = buffer.substring(0, buffer.length - 1);
      }
      break;
    case "+":
    case "-":
    case "×":
    case "÷":
      handleMath(value);
      break;
      case buffer.length > 8:
          console.log('test me');
          break;
      
  }
}


function rerender() {
  screen.innerText = buffer;
  console.log('is this now?');
}



function init() {
  document.querySelector(".calc-buttons").addEventListener("click", function(event) {
    buttonClick(event.target.innerText);
    console.log('this is the event.target.innerText ' + event.target.innerText);
    console.log('previous operator / value stored is now: ' + runningTotal);
    console.log('the length of the string from the buffer is now: ' + buffer.length);
    console.log('this is the output to the screen from the buffer: ' + buffer);
    //console.log('this is the limit ' + screenLimit);
    //console.log('this is the number of digits ' + numOfDigits);
  });
}

init();

