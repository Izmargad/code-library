/*
Izzi 2019-05-26
This Izzi's my calculator

Build a function to clear an entry to the screen
Build a function to add an entry to the screen
Build a function to parse the entry on the screen to a number
Build a function to do the math 

Tupperware for value entered tally
Tupperware for operator
Tupperware for another value added to the 

*/

let tally = 0; // this is the current value when the machine is switched on
let tallyDisplay = '0'; // this is the tally displayed on the machine's screen
let lastOperatorKey; // this tracks the operator key pressed
const screen = document.querySelector(".screen");//this is the constant element for display purposes

/*
Function to listen to button clicked
*/
function trackButtonClicks (){

    document.querySelector(".calc-buttons").addEventListener("click", function(event) {
        buttonClick(event.target.innerText);
        console.log('this is the event.target.innerText ' + event.target.innerText);
    });
}

trackButtonClicks();

function buttonClick() {
    // just listen for now
    console.log('is this now?');
  }