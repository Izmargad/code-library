# How to check for text in a string with vanilla JS

Let’s say you have a string, like this.

```javascript
var hitchhiker = 'The answer to the ultimate question of life, the universe, and everything';
```

You want to check if the string has the word question in it. How would you do that?

Today, we’re going to look at two ways to check for a string inside another string.

## The String.includes() method

ES6 introduced a new method for checking for a string inside another string: *String.includes()*.

Call the *includes()* method on your string, and pass in the substring you’re looking for inside it as an argument. It returns a boolean (*true/false*).

```javascript
// returns true
hitchiker.includes('question');

// returns false
hitchiker.includes('Neville');
```

### See example1.js

File inside this folder

The *String.includes()* method works in all modern browsers, but not IE. You can push support back to at least IE6 with a polyfill.

## The String.indexOf() method

The String.indexOf() method is an older approach that tells you the starting index of a substring inside a string. If the substring doesn’t exist, it returns *-1* instead.

```javascript
// returns 27
hitchhiker.indexOf('question');

// returns -1
hitchhiker.indexOf('Neville');

You can use the indexOf() method to check if a substring exists or not by checking if the returned value is greater than -1.

// This logs to the console
if (hitchhiker.indexOf('question') > -1) {
 console.log(42);
}

// This does not
if (hitchhiker.indexOf('Neville') > -1) {
 console.log('This is not Harry Potter, you idiot!');
}
```

### See example2.js

## A “gotcha” with both of these

Neither the *String.includes()* method nor *String.indexOf()* method check if the substring is a complete/standalone word.

For example, let’s check for the word quest. It’s not a word in our sentence, but it is part of the word question.

It return true with the *includes()* method, and 27 (just like question does) with the *indexOf() *method.

```javascript
// returns true
hitchiker.includes('quest');

// returns 27
hitchhiker.indexOf('quest');
```

Checking for complete words requires the use of regular expressions, and that’s an entirely [separate article](https://gomakethings.com/regex-with-javascript/).

## Which one should you use

Today, I probably lean more towards String.includes(), with a polyfill. It’s rare that I actually need the index of the substring. I generally just want to know if it exists or not.

If you don’t want to bother with the polyfill, String.indexOf() still gets the job done, though!

[Go Make Things: 9 March 2020](https://gomakethings.com/how-to-check-for-text-in-a-string-with-vanilla-js/)
