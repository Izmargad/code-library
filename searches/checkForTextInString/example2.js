var hitchhiker = 'The answer to the ultimate question of life, the universe, and everything';

console.log(hitchhiker.indexOf('question'));
console.log(hitchhiker.indexOf('Neville'));

// This logs to the console
if (hitchhiker.indexOf('question') > -1) {
	console.log(42);
}

// This does not
if (hitchhiker.indexOf('Neville') > -1) {
	console.log('This is not Harry Potter, you idiot!');
}