
## InnerHTML

The innerHTML property allows to get the HTML inside the element as a string.
We can also modify it. So it’s one of most powerful ways to change the page.
The example shows the contents of document.body and then replaces it
completely:
We can try to insert invalid HTML, the browser will fix our errors:

## Scripts don’t execute

If innerHTML inserts a <script> tag into the document – it becomes a
part of HTML, but doesn’t execute.
Beware: “innerHTML+=” does a full overwrite
We can append HTML to an element by using elem.innerHTML+="more
html".

Like this:

```html
<body>
<p>A paragraph</p>
<div>A div</div>
<script>
alert( document.body.innerHTML ); // read the current contents
document.body.innerHTML = 'The new BODY!'; // replace it
</script>
</body>
```
