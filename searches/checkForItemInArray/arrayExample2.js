var wizards = ['Hermoine', 'Neville', 'Harry Potter', 'Dumbledore'];

// logs 1
console.log(wizards.indexOf('Neville'));

// logs -1
console.log(wizards.indexOf('Gandalf'));

// This logs to the console
if (wizards.indexOf('Neville') > -1) {
	console.log('The surprise hero');
}

// This does not
if (wizards.indexOf('Gandalf') > -1) {
	console.log('This is not Lord of the Rings, you idiot!');
}