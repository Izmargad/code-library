var wizards = ['Hermoine', 'Neville', 'Harry Potter', 'Dumbledore'];

// logs true
console.log(wizards.includes('Neville'));

// logs false
console.log(wizards.includes('Gandalf'));